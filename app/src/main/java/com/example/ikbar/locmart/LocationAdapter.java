package com.example.ikbar.locmart;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.ikbar.locmart.Models.StoreLocation;
import com.example.ikbar.locmart.apihelper.BaseApiService;
import com.example.ikbar.locmart.apihelper.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by IKBAR on 12/13/2017.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {
    private Context lContext;
    private ArrayList<StoreLocation> locCo;
    String googleMap = "com.google.android.apps.maps";
    Uri gmmIntentUri;


    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView address;

        public MyViewHolder(View v){
            super(v);
            address = (TextView) v.findViewById(R.id.address);
        }
    }

    public LocationAdapter(ArrayList<StoreLocation> obj){
        locCo = obj;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.address.setText(locCo.get(position).getAddress());

        final String locCo2 = locCo.get(position).getCoordinate();
        holder.address.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                gmmIntentUri = Uri.parse("google.navigation:q=" + locCo2);
                Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                intent.setPackage(googleMap);
                v.getContext().startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (locCo == null) ? 0 : locCo.size();
    }
}
