package com.example.ikbar.locmart.AllProduct;

/**
 * Created by IKBAR on 11/8/2017.
 */

public class ProductModel {
    private String title, price, description;
    private int thumbnail;

    public ProductModel(){

    }

    public ProductModel(String title, String price, String description, int thumbnail){
        this.title = title;
        this.price = price;
        this.description = description;
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
