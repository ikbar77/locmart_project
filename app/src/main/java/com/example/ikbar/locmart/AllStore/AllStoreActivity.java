package com.example.ikbar.locmart.AllStore;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;

import com.example.ikbar.locmart.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IKBAR on 11/22/2017.
 */

public class AllStoreActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private StoreAdapter storeAdapter;
    private List<StoreModel> storeList;
    private ImageButton imageButton;

    public static AllStoreActivity newInstance(){
        return new AllStoreActivity();
    }

    @Nullable
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_store);

        imageButton = (ImageButton) findViewById(R.id.img_store);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_store);
        storeList = new ArrayList<>();
        storeAdapter = new StoreAdapter(this, storeList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new AllStoreActivity.GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(storeAdapter);

        prepareStore();

    }

    private void prepareStore() {
        int[] image = new int[]{
                R.drawable.alfamart,
                R.drawable.giant,
                R.drawable.hypermat,
                R.drawable.indomaret,
                R.drawable.lottemat,
                R.drawable.superindo,
        };

        StoreModel a = new StoreModel(1, "Alfamart", image[0]);
        storeList.add(a);

        a = new StoreModel(2, "Giant", image[1]);
        storeList.add(a);

        a = new StoreModel(3, "Hypermart", image[2]);
        storeList.add(a);

        a = new StoreModel(4, "Indomart", image[3]);
        storeList.add(a);

        a = new StoreModel(5, "Lottemart", image[4]);
        storeList.add(a);

        a = new StoreModel(6, "Super Indo", image[5]);
        storeList.add(a);


        storeAdapter.notifyDataSetChanged();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
