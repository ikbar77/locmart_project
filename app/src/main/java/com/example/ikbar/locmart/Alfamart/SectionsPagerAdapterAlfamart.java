package com.example.ikbar.locmart.Alfamart;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.ikbar.locmart.Alfamart.AlfamartLocation;
import com.example.ikbar.locmart.Alfamart.AlfamartProduct;

/**
 * Created by IKBAR on 10/8/2017.
 */

public class SectionsPagerAdapterAlfamart extends FragmentStatePagerAdapter {
    int numOfTabs;

    public SectionsPagerAdapterAlfamart(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                AlfamartProduct tab1 = new AlfamartProduct();
                return tab1;
            case 1:
                AlfamartLocation tab2 = new AlfamartLocation();
                return tab2;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

}
