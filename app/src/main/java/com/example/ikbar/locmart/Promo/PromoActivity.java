package com.example.ikbar.locmart.Promo;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.example.ikbar.locmart.AllProduct.AllProductActivity;
import com.example.ikbar.locmart.AllProduct.ProductAdapter;
import com.example.ikbar.locmart.DividerItemDecoration;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.Promo;
import com.example.ikbar.locmart.R;
import com.example.ikbar.locmart.apihelper.BaseApiService;
import com.example.ikbar.locmart.apihelper.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IKBAR on 11/22/2017.
 */

public class PromoActivity extends AppCompatActivity {

    private ArrayList<Promo> promoList ;
    private RecyclerView recyclerView;
    private PromoAdapter promoAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_promo);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_all_promo);
        promoAdapter = new PromoAdapter(this, promoList);
        BaseApiService mApiService = UtilsApi.getAPIService(this);

        RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(pLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));

        recyclerView.setAdapter(promoAdapter);

        Call<ArrayList<Promo>> productCall = mApiService.promo();
        productCall.enqueue(new Callback<ArrayList<Promo>>() {
            @Override
            public void onResponse(Call<ArrayList<Promo>> call, Response<ArrayList<Promo>> response) {
                try {
                    promoList = response.body();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                promoAdapter = new PromoAdapter(getApplicationContext(), promoList);

                RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(pLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL, 16));

                recyclerView.setAdapter(promoAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<Promo>> call, Throwable t) {
                t.printStackTrace();
            }
        });


//        preparePromo();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
