package com.example.ikbar.locmart;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.ikbar.locmart.Alfamart.AlfamartActivity;
import com.example.ikbar.locmart.AllProduct.AllProductActivity;
import com.example.ikbar.locmart.AllProduct.ProductAdapter;
import com.example.ikbar.locmart.AllProduct.ProductModel;
import com.example.ikbar.locmart.AllStore.AllStoreActivity;
import com.example.ikbar.locmart.Giant.GiantActivity;
import com.example.ikbar.locmart.Hypermart.HypermartActivity;
import com.example.ikbar.locmart.Indomart.IndomartActivity;
import com.example.ikbar.locmart.Lottemart.LottemartActivity;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.Promo;
import com.example.ikbar.locmart.Models.StoreLocation;
import com.example.ikbar.locmart.Promo.PromoActivity;
import com.example.ikbar.locmart.Promo.PromoAdapter;
import com.example.ikbar.locmart.Superindo.SuperindoActivity;
import com.example.ikbar.locmart._sliders.FragmentSlider;
import com.example.ikbar.locmart._sliders.SliderIndicator;
import com.example.ikbar.locmart._sliders.SliderPagerAdapter;
import com.example.ikbar.locmart._sliders.SliderView;
import com.example.ikbar.locmart.apihelper.BaseApiService;
import com.example.ikbar.locmart.apihelper.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Button btn_giant, btn_superindo, btn_hypermart, btn_lottemart;
    private ImageView btn_indomart, btn_alfamart;

    private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;
    private SliderView sliderView;
    private LinearLayout mLinearLayout;

    private ArrayList<Promo> promoList = new ArrayList<>();
    private List<BerandaModel> priceList = new ArrayList<>();
    private RecyclerView recyclerView, recyclerView2;
    private PromoAdapter promoAdapter;
    private BerandaAdapter bAdapter;

    Context mContext;
    BaseApiService mApiService;
    Retrofit retrofit;

    private ArrayList<StoreLocation> coordinate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        ((TextView) findViewById(R.id.main_toolbar_title)).setText(R.string.app_name);

        mContext = this;
        mApiService = UtilsApi.getAPIService(this);

        btn_alfamart = (ImageView) findViewById(R.id.alfamart_btn);
        btn_indomart = (ImageView) findViewById(R.id.indomaret_btn);
        btn_giant = (Button) findViewById(R.id.giant_btn);
        btn_hypermart = (Button) findViewById(R.id.hypermart_btn);
        btn_lottemart = (Button) findViewById(R.id.lottemart_btn);
        btn_superindo = (Button) findViewById(R.id.superindo_btn);

        preparePriceData();

        Call<ArrayList<Promo>> promoCall = mApiService.promo();
        promoCall.enqueue(new Callback<ArrayList<Promo>>() {
            @Override
            public void onResponse(Call<ArrayList<Promo>> call, Response<ArrayList<Promo>> response) {
                try {
                    promoList = response.body();
                    Log.d("Hasil ", "promoList : " + promoList.size());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Promo>> call, Throwable t) {

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        sliderView = (SliderView) findViewById(R.id.sliderView);
        mLinearLayout = (LinearLayout) findViewById(R.id.pagesContainer);
        setupSlider();

        btn_alfamart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AlfamartActivity.class);
                startActivity(intent);
            }
        });

        btn_indomart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, IndomartActivity.class);
                startActivity(intent);
            }
        });

        btn_giant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GiantActivity.class);
                startActivity(intent);
            }
        });

        btn_hypermart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HypermartActivity.class);
                startActivity(intent);
            }
        });

        btn_lottemart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LottemartActivity.class);
                startActivity(intent);
            }
        });

        btn_superindo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SuperindoActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main, menu);
//        //      getMenuInflater().inflate(R.menu.main, menu);
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_promo) {
            Intent intent = new Intent(MainActivity.this, PromoActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_store) {
            Intent intent = new Intent(MainActivity.this, AllStoreActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_product) {
            Intent intent = new Intent(MainActivity.this, AllProductActivity.class);
            startActivity(intent);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void setupSlider() {
        sliderView.setDurationScroll(800);
        List<Fragment> fragments = new ArrayList<>();
        String url = "http://madamita.ml/bryan/apiLocmart/backend/web/";
        for (int i=0; i<promoList.size(); i++){
            Log.d("url", "Promo: " + url + promoList.get(i).getPromo_img());
        }
        fragments.add(FragmentSlider.newInstance("http://madamita.ml/bryan/apiLocmart/backend/web/uploads/store/promosatu1128.jpg"));
        fragments.add(FragmentSlider.newInstance("http://madamita.ml/bryan/apiLocmart/backend/web/uploads/store/sgsfg417.jpg"));
        fragments.add(FragmentSlider.newInstance("http://madamita.ml/bryan/apiLocmart/backend/web/uploads/store/fdgs2648.jpg"));
        fragments.add(FragmentSlider.newInstance("http://madamita.ml/bryan/apiLocmart/backend/web/uploads/store/eee2446.jpeg"));

        mAdapter = new SliderPagerAdapter(getSupportFragmentManager(), fragments);
        sliderView.setAdapter(mAdapter);
        mIndicator = new SliderIndicator(this, mLinearLayout, sliderView, R.drawable.indicator_circle);
        mIndicator.setPageCount(fragments.size());
        mIndicator.show();
    }

    private void preparePriceData() {
        int[] image = new int[]{
                R.drawable.indomaret,
                R.drawable.alfamart,
                R.drawable.superindo,
                R.drawable.lottemat,
                R.drawable.giant,
                R.drawable.hypermat,
        };

        BerandaModel item2 = new BerandaModel(1, "Indomart", "20000", image[0]);
        priceList.add(item2);

        item2 = new BerandaModel(2, "Alfamart", "10000", image[1]);
        priceList.add(item2);

        item2 = new BerandaModel(3, "SuperIndo", "11000", image[2]);
        priceList.add(item2);

        item2 = new BerandaModel(4, "Lottemart", "11500", image[3]);
        priceList.add(item2);

        item2 = new BerandaModel(5, "Giant", "9500", image[4]);
        priceList.add(item2);

        item2 = new BerandaModel(6, "Hypermart", "12000", image[5]);
        priceList.add(item2);
    }
}
