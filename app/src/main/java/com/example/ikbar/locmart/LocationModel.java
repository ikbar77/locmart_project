package com.example.ikbar.locmart;

/**
 * Created by IKBAR on 12/13/2017.
 */

public class LocationModel {
    private int location_id;
    private String address, city, phone;

    public LocationModel(){

    }

    public LocationModel(int location_id, String address, String city, String phone){
        this.location_id = location_id;
        this.address = address;
        this.city = city;
        this.phone = phone;

    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
