package com.example.ikbar.locmart.apihelper;

import android.content.Context;

import com.example.ikbar.locmart.R;

/**
 * Created by Escanor on 12/18/2017.
 */

public class UtilsApi {
    // 192.168.1.8 ini adalah localhost
    //public static final String BASE_URL_API = "http://192.168.1.8/LocMartAPI/backend/web/";
    //public static final String BASE_URL_API = "http://192.168.43.91/LocMartAPI/backend/web/";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService(Context context){
        String url = context.getString(R.string.BASE_URL);
        return RetrofitClient.getClient(url).create(BaseApiService.class);
    }
}
