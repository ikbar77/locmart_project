package com.example.ikbar.locmart;

/**
 * Created by IKBAR on 12/19/2017.
 */

public class BerandaModel {
    private int id, thumbnail;
    private String store_name, price;

    public BerandaModel(){

    }

    public BerandaModel(int id, String store_name, String price, int thumbnail){
        this.id = id;
        this.price = price;
        this.store_name = store_name;
        this.thumbnail = thumbnail;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
