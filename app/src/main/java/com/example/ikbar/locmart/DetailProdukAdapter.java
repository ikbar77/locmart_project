package com.example.ikbar.locmart;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ikbar.locmart.AllProduct.ProductModel;
import com.example.ikbar.locmart.Models.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IKBAR on 12/17/2017.
 */

public class DetailProdukAdapter extends RecyclerView.Adapter<DetailProdukAdapter.MyViewHolder> {
    private Context dContext;
    private List<Product> productList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView description, produk_title, product_price;

        public MyViewHolder(View v){
            super(v);
            description = (TextView) v.findViewById(R.id.produk_description);
            produk_title = (TextView) v.findViewById(R.id.produk_title);
            product_price = (TextView) v.findViewById(R.id.produk_price);
        }
    }

    public DetailProdukAdapter(Context context, ArrayList<Product> obj){
        this.dContext = context;
        this.productList = obj;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_produk_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.description.setText(productList.get(position).getProduct_description());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
