package com.example.ikbar.locmart.apihelper;

import com.example.ikbar.locmart.LocationModel;
import com.example.ikbar.locmart.Models.Category;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.Promo;
import com.example.ikbar.locmart.Models.Store;
import com.example.ikbar.locmart.Models.StoreLocation;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Escanor on 12/18/2017.
 */

public interface BaseApiService {

    @GET("api/web/store")
    Call<ArrayList<Store>> store();

    @GET("api/web/store-location")
    Call<ArrayList<StoreLocation>> storeLoc();

    @GET("api/web/product")
    Call<ArrayList<Product>> allproduct();

    @GET("api/web/promo")
    Call<ArrayList<Promo>> promo();

    @GET("api/web/category")
    Call<ArrayList<Category>> category();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=1")
    Call<ArrayList<StoreLocation>> locationIndo();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=2")
    Call<ArrayList<StoreLocation>> locationAlfa();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=3")
    Call<ArrayList<StoreLocation>> locationGiant();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=4")
    Call<ArrayList<StoreLocation>> locationHyper();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=5")
    Call<ArrayList<StoreLocation>> locationCarre();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=6")
    Call<ArrayList<StoreLocation>> locationSuper();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=1")
    Call<ArrayList<StoreLocation>> locationLotte();

    @GET("api/web/store-location/search?StoreLocationSearch[STORE_ID]=1")
    Call<ArrayList<StoreLocation>> locationRanch();

    @GET("api/web/product/search")
    Call<ArrayList<Product>> productAlfamart(@Query("ProductSearch[STORE_ID]") Integer id,
                                             @Query("ProductSearch[CATEGORY_ID]") Integer idCategory);

    @GET("api/web/product/search")
    Call<ArrayList<Product>> products(@Query("ProductSearch[STORE_ID]") Integer id);

    @GET("api/web/product/search")
    Call<ArrayList<Product>> productdetail(@Query("ProductSearch[PRODUCT_ID]") String productid);

    @GET("api/web/product/search")
    Call<ArrayList<Product>> listproduct(@Query("ProductSearch[PRODUCT_NAME]") String name );
}
