package com.example.ikbar.locmart.AllStore;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ikbar.locmart.Alfamart.AlfamartActivity;
import com.example.ikbar.locmart.Giant.GiantActivity;
import com.example.ikbar.locmart.Hypermart.HypermartActivity;
import com.example.ikbar.locmart.Indomart.IndomartActivity;
import com.example.ikbar.locmart.Lottemart.LottemartActivity;
import com.example.ikbar.locmart.R;
import com.example.ikbar.locmart.Superindo.SuperindoActivity;

import java.util.List;

/**
 * Created by IKBAR on 11/22/2017.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {
    private Context sContext;
    private List<StoreModel> storeModelList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView storeName;
        private ImageButton imgStore;

        MyViewHolder(View v) {
            super(v);
            storeName = (TextView) v.findViewById(R.id.store_title);
            imgStore = (ImageButton) v.findViewById(R.id.img_store);
        }
    }

    public StoreAdapter(Context sContext, List<StoreModel> storeModelList) {
        this.sContext = sContext;
        this.storeModelList = storeModelList;
    }

    @Override
    public StoreAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_store_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final StoreAdapter.MyViewHolder holder, int position) {
        StoreModel store = storeModelList.get(position);
        holder.storeName.setText(store.getStoreName());
        final int id = store.getId();
//        holder.title.setText(product.getTitle());
//        holder.price.setText(product.getPrice());

        Glide.with(sContext).load(store.getImgStore()).into(holder.imgStore);
        holder.imgStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id == 1) {
                    Intent intent = new Intent(v.getContext(), AlfamartActivity.class);
                    v.getContext().startActivity(intent);
                }else if (id == 2){
                    Intent intent = new Intent(v.getContext(), GiantActivity.class);
                    v.getContext().startActivity(intent);
                }else if(id == 3){
                    Intent intent = new Intent(v.getContext(), HypermartActivity.class);
                    v.getContext().startActivity(intent);
                }else if(id == 4){
                    Intent intent = new Intent(v.getContext(), IndomartActivity.class);
                    v.getContext().startActivity(intent);
                }else if(id == 5){
                    Intent intent = new Intent(v.getContext(), LottemartActivity.class);
                    v.getContext().startActivity(intent);
                }else if(id == 6){
                    Intent intent = new Intent(v.getContext(), SuperindoActivity.class);
                    v.getContext().startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return storeModelList.size();
    }
}
