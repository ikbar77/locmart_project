package com.example.ikbar.locmart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ikbar.locmart.AllProduct.ProductModel;

import java.util.List;

/**
 * Created by IKBAR on 12/19/2017.
 */

public class BerandaAdapter extends RecyclerView.Adapter<BerandaAdapter.MyViewHolder> {
    private Context bContext;
    private List<BerandaModel> priceList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView title, price;
        public ImageView thumbnail;

        public MyViewHolder(View v){
            super(v);
            title = (TextView) v.findViewById(R.id.pb_title);
            price = (TextView) v.findViewById(R.id.pb_price);
            thumbnail = (ImageView) v.findViewById(R.id.thumbnail2);
        }
    }

    public BerandaAdapter(Context bContext, List<BerandaModel> priceList){
        this.bContext = bContext;
        this.priceList = priceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.beranda_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BerandaModel beranda = priceList.get(position);
        holder.title.setText(beranda.getStore_name());
        holder.price.setText("Rp " + beranda.getPrice()+",-");
        Glide.with(bContext).load(beranda.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return priceList.size();
    }
}
