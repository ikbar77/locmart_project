package com.example.ikbar.locmart;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ikbar.locmart.Alfamart.AlfamartProduct;
import com.example.ikbar.locmart.AllProduct.AllProductActivity;
import com.example.ikbar.locmart.AllProduct.ProductAdapter;
import com.example.ikbar.locmart.AllProduct.ProductModel;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.Store;
import com.example.ikbar.locmart.apihelper.BaseApiService;
import com.example.ikbar.locmart.apihelper.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by IKBAR on 12/17/2017.
 */

public class DetailProductActivity extends AppCompatActivity {
    private RecyclerView recyclerView, recyclerView2;
    private PriceListAdapter priceListAdapter;
    private ArrayList<Store> storeList;
    private ArrayList<Product> productList;
    private ArrayList<Product> listprice;
    private ImageView imgDetail;
    private TextView tv1, tv2, tv3;
    private String productId;
    BaseApiService mApiService;
    Bundle bundle;
    String nameStore;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_product);

        imgDetail = (ImageView) findViewById(R.id.img_detail_product);
        tv1 = (TextView) findViewById(R.id.produk_title);
        tv2 = (TextView) findViewById(R.id.produk_price);
        tv3 = (TextView) findViewById(R.id.produk_description);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_list_harga);
        mApiService = UtilsApi.getAPIService(this);
        final String url = this.getString(R.string.BASE_URL);

        bundle = getIntent().getExtras();
        productId = bundle.getString("productId");

        Log.d("productID", "onCreate:  " + productId);

        ActionBar menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setDisplayHomeAsUpEnabled(true);

        final Call<ArrayList<Product>> productCall = mApiService.productdetail(productId);

        productCall.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
                try {
                    productList = response.body();
                    Log.d(TAG, "productList: " + productList.get(0).getProduct_name());
                }catch (Exception e){
                    e.printStackTrace();
                }

                tv1.setText(productList.get(0).getProduct_name());
                tv2.setText("Rp. " + productList.get(0).getProduct_price());
                tv3.setText(productList.get(0).getProduct_description());
                Glide.with(DetailProductActivity.this).load(url + "backend/web/" + productList.get(0).getProduct_img()).into(imgDetail);

                nameStore = productList.get(0).getProduct_name();

                final Call<ArrayList<Product>> productlistCall = mApiService.listproduct(nameStore);
                productlistCall.enqueue(new Callback<ArrayList<Product>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
                        try {
                            listprice = response.body();
                            Log.d(TAG, "productList: " + listprice.get(0).getProduct_name());
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        final Call<ArrayList<Store>> imgstoreCall = mApiService.store();
                        imgstoreCall.enqueue(new Callback<ArrayList<Store>>() {
                            @Override
                            public void onResponse(Call<ArrayList<Store>> call, Response<ArrayList<Store>> response) {
                                try {
                                    storeList = response.body();
                                    Log.d(TAG, "productList: " + storeList.get(0).getStore_name());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                                priceListAdapter = new PriceListAdapter(DetailProductActivity.this,storeList, listprice);
                                RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(pLayoutManager);
                                recyclerView.addItemDecoration(new DividerItemDecoration(DetailProductActivity.this, LinearLayoutManager.VERTICAL, 0));
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(priceListAdapter);
                            }

                            @Override
                            public void onFailure(Call<ArrayList<Store>> call, Throwable t) {
                                t.printStackTrace();
                            }
                        });

                    }

                    @Override
                    public void onFailure(Call<ArrayList<Product>> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

            }

            @Override
            public void onFailure(Call<ArrayList<Product>> call, Throwable t) {
                t.printStackTrace();
            }
        });




    }


    private void prepareProduct() {
        int[] image = new int[]{
                R.drawable.alfamart,
                R.drawable.indomaret,
                R.drawable.lottemat,
                R.drawable.hypermat,
                R.drawable.superindo,
                R.drawable.giant,
        };

//        ProductModel a = new ProductModel("Pringles", "15.000", "ahsdhkjash jhkjashdkjh kjahskjdh", image[0]);
//        productList.add(a);
//
//        a = new ProductModel("Pringles", "14.000", "ahsdhkjash", image[1]);
//        productList.add(a);
//
//        a = new ProductModel("Pringles", "13.000", "ahsdhkjash", image[2]);
//        productList.add(a);
//
//        a = new ProductModel("Pringles", "14.500", "ahsdhkjash", image[3]);
//        productList.add(a);
//
//        a = new ProductModel("Pringles", "14.000", "ahsdhkjash", image[4]);
//        productList.add(a);
//
//        a = new ProductModel("Pringles", "13.500", "ahsdhkjash", image[5]);
//        productList.add(a);
//
//        detailProdukAdapter.notifyDataSetChanged();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing + 20;
                }
                outRect.bottom = spacing + 20; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing + 20; // item top
                }
            }
        }
    }


    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
