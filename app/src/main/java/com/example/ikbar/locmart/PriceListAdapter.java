package com.example.ikbar.locmart;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ikbar.locmart.AllProduct.ProductModel;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.Store;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IKBAR on 1/15/2018.
 */

public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.MyViewHolder> {
    private Context priceContext;
    private List<Product> priceList;
    private List<Store> storeList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_list_price;
        private TextView txt_list_price;

        public MyViewHolder(View v) {
            super(v);
            img_list_price = (ImageView) v.findViewById(R.id.img_list_price);
            txt_list_price = (TextView) v.findViewById(R.id.txt_list_price);
        }
    }

    public PriceListAdapter(Context context, List<Store> obj1, List<Product> obj2) {
        priceContext = context;
        storeList = obj1;
        priceList = obj2;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_harga_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String url = priceContext.getString(R.string.BASE_URL);
        Store store = new Store();
        store = storeList.get(position);
        Product price = new Product();
        price = priceList.get(position);

        Glide.with(priceContext).load(url + "backend/web/" + store.getStore_img()).into(holder.img_list_price);
        Log.d("Image", "url gambar: " + url + "backend/web/" + store.getStore_img());
        holder.txt_list_price.setText("Rp "+ price.getProduct_price()+",-");
    }

    @Override
    public int getItemCount() {
        return priceList.size();
    }
}
