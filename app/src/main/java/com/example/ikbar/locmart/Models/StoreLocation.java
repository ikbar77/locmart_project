package com.example.ikbar.locmart.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Escanor on 12/20/2017.
 */

public class StoreLocation {

    @SerializedName("LOCATION_ID")
    private String location_id;
    @SerializedName("STORE_ID")
    private String store_id;
    @SerializedName("NAME")
    private String location_name;
    @SerializedName("ADDRESS")
    private String address;
    @SerializedName("COORDINATE")
    private String coordinate;
    @SerializedName("PHONE")
    private String phone;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getStore_id() {
        return store_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
