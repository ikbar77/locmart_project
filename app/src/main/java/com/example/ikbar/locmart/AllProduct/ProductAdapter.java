package com.example.ikbar.locmart.AllProduct;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ikbar.locmart.DetailProductActivity;
import com.example.ikbar.locmart.MainActivity;
import com.example.ikbar.locmart.Models.Product;
import com.example.ikbar.locmart.Models.StoreLocation;
import com.example.ikbar.locmart.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by IKBAR on 11/8/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<Product> productList;
    private String productId;

    public ProductAdapter(Context context, ArrayList<Product> obj) {
        mContext = context;
        productList = obj;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView title, price;
        public ImageView thumbnail;

        public MyViewHolder(View v){
            super(v);
            title = (TextView) v.findViewById(R.id.p_title);
            price = (TextView) v.findViewById(R.id.p_price);
            thumbnail = (ImageView) v.findViewById(R.id.thumbnail);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_product_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        String url = mContext.getString(R.string.BASE_URL);

        final Product product = productList.get(position);

        productId = product.getProduct_id();
        holder.title.setText(product.getProduct_name());
        holder.price.setText("Rp " + product.getProduct_price()+",-");
        //TODO: ini cara ngambil gambar dr database
        //holder.thumbnail.
        Glide.with(mContext).load(url + "backend/web/" + product.getProduct_img()).into(holder.thumbnail);
        Log.d(TAG, "onBindViewHolder: " + url + "backend/web/" + product.getProduct_img());
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailProductActivity.class);
                intent.putExtra("productId", product.getProduct_id());
                Log.d("productId", "PRODUK: " + productId);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (productList == null) ? 0 : productList.size();
    }
}
