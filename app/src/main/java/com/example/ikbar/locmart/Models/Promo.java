package com.example.ikbar.locmart.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by IKBAR on 1/5/2018.
 */

public class Promo {
    @SerializedName("PROMO_ID")
    private String promo_id;
    @SerializedName("STORE_ID")
    private String store_id;
    @SerializedName("PROMO_IMG")
    private String promo_img;

    public String getPromo_id() {
        return promo_id;
    }

    public void setPromo_id(String promo_id) {
        this.promo_id = promo_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getPromo_img() {
        return promo_img;
    }

    public void setPromo_img(String promo_img) {
        this.promo_img = promo_img;
    }
}
