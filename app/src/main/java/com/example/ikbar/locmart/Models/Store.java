package com.example.ikbar.locmart.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Escanor on 12/20/2017.
 */

public class Store {
    @SerializedName("STORE_ID")
    private String store_id;
    private String location_id;
    @SerializedName("STORE_NAME")
    private String store_name;
    @SerializedName("STORE_IMG")
    private String store_img;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_img() {
        return store_img;
    }

    public void setStore_img(String store_img) {
        this.store_img = store_img;
    }
}
