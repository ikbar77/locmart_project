package com.example.ikbar.locmart.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Escanor on 1/15/2018.
 */

public class Category {
    @SerializedName("CATEGORY_ID")
    private int category_id;
    @SerializedName("CATEGORY_NAME")
    private String category_name;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }


}
